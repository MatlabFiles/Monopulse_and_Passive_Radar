function MMSE=ComputeMMSE(Average,All_ThetaT,All_PhiT)
%% Computes the Mean Square Error in the Elevation (Theta in degrees) and Azimuth (Phi in degrees)
Average=Average*180/pi;
All_ThetaT=All_ThetaT*180/pi;
All_PhiT=All_PhiT*180/pi;
MMSE(:,1)=0.5*(mean((Average(:,:,1)-All_ThetaT(:,:,1)).^2,2)+mean((Average(:,:,3)-All_ThetaT(:,:,2)).^2,2));
MMSE(:,2)=0.5*(mean((Average(:,:,2)-All_PhiT  (:,:,1)).^2,2)+mean((Average(:,:,4)-All_PhiT  (:,:,2)).^2,2));
