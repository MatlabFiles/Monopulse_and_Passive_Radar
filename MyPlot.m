function MyPlot(Angles,ThetaT,PhiT,Average,Title)
axis([-90 90 -90 90]); set(gca,'XTick',-90:15:90,'YTick',-90:15:90,'XMinorTick','on'); grid on 
xlabel('Elevation Angle $\theta$','Interpreter','latex');
ylabel('Azimuth Angle $\phi$','Interpreter','latex');
title(Title)
hold on;
scatter(ThetaT*180/pi,PhiT*180/pi,50,'b')
scatter(Angles(:,1)*180/pi,Angles(:,2)*180/pi,10,'r')
scatter(Angles(:,3)*180/pi,Angles(:,4)*180/pi,10,'g')
scatter(Average([1,3])*180/pi,Average([2,4])*180/pi,50,'k*')
hold on; scatter(ThetaT*180/pi,PhiT*180/pi,50,'b')
legend('Targets','Estimates T1','Estimates T2','Averages','Location','best')
set(gcf,'Color','w')