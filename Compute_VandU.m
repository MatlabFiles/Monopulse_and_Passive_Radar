function [Sol_a,Sol_b]=Compute_VandU(Y,d_over_lambda,alpha,beta)
    Eq(1,:)=real(Y(1,:).*Y(4,:)-Y(2,:).*Y(3,:));
    Eq(2,:)=real(Y(1,:).*conj(Y(2,:))-Y(3,:).*conj(Y(4,:)));
    Eq(3,:)=imag(Y(1,:).*conj(Y(2,:))-Y(3,:).*conj(Y(4,:)));
    Eq(4,:)=real(Y(1,:).*conj(Y(3,:))-Y(2,:).*conj(Y(4,:)));
    Eq(5,:)=imag(Y(1,:).*conj(Y(3,:))-Y(2,:).*conj(Y(4,:)));
    Eq(6,:)=(abs(Y(1,:)).^2-abs(Y(4,:)).^2)+(abs(Y(2,:)).^2-abs(Y(3,:)).^2);
    Eq(7,:)=(abs(Y(1,:)).^2-abs(Y(4,:)).^2)-(abs(Y(2,:)).^2-abs(Y(3,:)).^2);
    Eq(8,:)=(abs(Y(1,:)).^2+abs(Y(4,:)).^2)-(abs(Y(2,:)).^2+abs(Y(3,:)).^2);

    a_Den=abs(Y(1,:)+Y(2,:)).^2-abs(Y(3,:)+Y(4,:)).^2;  %Eq(6,:) + 2 Eq(2,:)
    b_Den=abs(Y(1,:)+Y(3,:)).^2-abs(Y(2,:)+Y(4,:)).^2;  %Eq(7,:) + 2 Eq(4,:)
    
    a_Eq1=Eq(3,:)./Eq(2,:);     %tan(cos(alpha)*(a1+a2))
    a_Eq2=4*Eq(3,:)./a_Den;     %tan(cos(alpha)*a1)+tan(cos(alpha)*a2)

    b_Eq1=Eq(5,:)./Eq(4,:);     %tan(sin(alpha)*(b1+b2))
    b_Eq2=4*Eq(5,:)./b_Den;     %tan(sin(alpha)*b1)+tan(sin(alpha)*b2)
    
    Temp_Sol_b=real([
            atan((b_Eq2/2 + (b_Eq2.^2 - 4*(1-b_Eq2./b_Eq1)).^(1/2)/2)); ... 
            atan((b_Eq2/2 - (b_Eq2.^2 - 4*(1-b_Eq2./b_Eq1)).^(1/2)/2));
            ]);
        
    Temp_Sol_a=real([
            atan((a_Eq2/2 + (a_Eq2.^2 - 4*(1-a_Eq2./a_Eq1)).^(1/2)/2)); ... 
            atan((a_Eq2/2 - (a_Eq2.^2 - 4*(1-a_Eq2./a_Eq1)).^(1/2)/2));
            ]);
    
        
    temp=(Eq(7,:)./Eq(6,:).*tan(Temp_Sol_b(1,:)-Temp_Sol_b(2,:)).*cot(Temp_Sol_a(1,:)-Temp_Sol_a(2,:))<0);
    Temp_Sol_a(:,temp)=flipud(Temp_Sol_a(:,temp)); 
    
    Temp_Sol_b=Temp_Sol_b/sin(alpha);
    Temp_Sol_a=Temp_Sol_a/cos(alpha);
    
    Sol_b=1/(2*pi*d_over_lambda)*real(sin(beta)*Temp_Sol_a+cos(beta)*Temp_Sol_b);
    Sol_a=1/(2*pi*d_over_lambda)*real(cos(beta)*Temp_Sol_a-sin(beta)*Temp_Sol_b);    

    temp=Sol_b(1,:)-Sol_b(2,:)<0;
    Sol_b(:,temp)=flipud(Sol_b(:,temp));
    Sol_a(:,temp)=flipud(Sol_a(:,temp));
    
    %%%
%     temp=abs(Sol_b(1,:)-Sol_b(2,:))<0.1;
%     if sum(temp)>1/2*length(temp)
%         temp=Sol_a(1,:)-Sol_a(2,:)<0;
%         Sol_a(:,temp)=flipud(Sol_a(:,temp));
%         Sol_b(:,temp)=flipud(Sol_b(:,temp));
%     end