function [Estimates,Average]=SmartAverage(Estimates,MMSE,N)
	TH=3;
    k=1;
    l=1;
    while k<N
        l=l+1;
        if (MMSE(k,3)/MMSE(l,3)+MMSE(k,4)/MMSE(l,4)>TH)
            if MMSE(k,3)/MMSE(l,3)+MMSE(k,4)/MMSE(l,4)<inv(MMSE(k,3)/MMSE(l,3))+inv(MMSE(k,4)/MMSE(l,4))
                Estimates(:,3:4,l)=nan;
            else
                Estimates(:,3:4,k)=nan;
            end
        elseif (inv(MMSE(k,3)/MMSE(l,3))+inv(MMSE(k,4)/MMSE(l,4))>TH)
            Estimates(:,3:4,l)=nan;
        end

        if (MMSE(k,1)/MMSE(l,1)+MMSE(k,2)/MMSE(l,2)>TH)
            if MMSE(k,1)/MMSE(l,1)+MMSE(k,2)/MMSE(l,2)<inv(MMSE(k,1)/MMSE(l,1))+inv(MMSE(k,2)/MMSE(l,2))
                Estimates(:,1:2,l)=nan;
            else
                Estimates(:,1:2,k)=nan;
            end
        elseif (inv(MMSE(k,1)/MMSE(l,1))+inv(MMSE(k,2)/MMSE(l,2))>TH)
            Estimates(:,1:2,l)=nan;
        end
        if l==N
            k=k+1;
            l=k;
        end
    end
    
    Estimates=reshape(permute(Estimates,[1 3 2]),[],4);
    
    FiniteT1=isfinite(Estimates(:,2));
    FiniteT2=isfinite(Estimates(:,4));
    Average(1:2)=mean(Estimates(FiniteT1,[1,2]));
    Average(3:4)=mean(Estimates(FiniteT2,[3,4]));
