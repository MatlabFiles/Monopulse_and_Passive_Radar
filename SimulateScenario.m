function SimulateScenario(ThetaT,PhiT,NPulses,d_over_lambda,NoisePower,AntennaCoordinates,setting,alpha,beta)

[~,NumAntenna]=size(AntennaCoordinates);
[NumSubantenna,~]=size(setting);

Ex=[cos(ThetaT).*cos(PhiT) cos(ThetaT).*sin(PhiT) sin(ThetaT)]';
Beta=exp(1j*randn(2,NPulses));
Y=exp(1j*2*pi*d_over_lambda*AntennaCoordinates'*Ex)*Beta;
Y=Y+sqrt(NoisePower/2)*(randn(NumAntenna,NPulses)+1j*randn(NumAntenna,NPulses));

[Estimates,Average]=ComputeEstimates(Y,d_over_lambda,setting,alpha,beta);

for i=1:NumSubantenna
    figure;clf;MyPlot(Estimates(:,:,i),ThetaT,PhiT,Average(i,:),'')
end