# Matlab Codes for "Generalised two target localisation using passive monopulse radar"

Link to publication: http://digital-library.theiet.org/content/journals/10.1049/iet-rsn.2016.0495

After running the main.m file, 10 figures will be displayed for different scenarios.

In the first 6 figures, the __targets__ are indicated by a blue circle '<span style="color:blue">o</span>', __monopulse estimates__ by red and green circles, and __averages__ are drawn as '<span style="color:black">*</span>'.

__Figure 1__

- The antenna has 4 elements set up in a rectangular 'x' format.
- The two targets are on different elevation and azimuth angles.
- &rarr; The averages are good estimates of the targets


__Figure 2__

- The antenna has 4 elements set up in a rectangular '+' format.
- The two targets are on different elevation and azimuth angles.
- &rarr; The averages are good estimates of the targets


__Figure 3__

- The antenna has 4 elements set up in a rectangular 'x' format.
- The two targets share the same azimuth angle \phi = 30.
- &rarr; The averages fail to estimate the target locations

__Figure 4__

- The antenna has 4 elements set up in a rectangular '+' format.
- The two targets share the same azimuth angle \phi = 30.
- &rarr; Because we changed the antenna setting, although the two targets share the same azimuth angle, the averages are now able to estimate the target locations

__Figure 5 & 6__

- A similar bihaviour is noticed when the two targets share the same elevation angle \theta = 0.
- In Figure 5, the estimates are on the line defined by \theta = 0.
- In Figure 6, the estimates are concentrated around the actual targets. In the algorithm, target 1 is identified as the one having higher elevation angle (without loss of generality). In this scenario, since both targets share the same elevation angle, then to identify the targets, it would be wise to add that the target1 has higher azimuth angle for example.

__Figure 7 & 8__

- Here, the performance of an antenna with 6 element is studied.
- We showed the MSE of both \phi and \theta as function of the angular distance between the two targets.
- The performance of the __proposed smart averaging__ is compared with the performance of separate sets of antennas and the simple averaging method.

__Figure 9 & 10__

- Here, instead of 6 elements we showed the performance of an 8 element antenna.

Have a nice day, 
