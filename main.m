clc; clear; close all

NPulses=50;
d_over_lambda=1/4;
NoisePower=10^(-3);

ThetaT = [ 45 ; -30]  *pi/180;
PhiT   = [-15 ;  30]  *pi/180;


Angles= [ pi/4 3*pi/4 -3*pi/4 -pi/4];
AntennaCoordinates=[zeros(1,length(Angles));cos(Angles);sin(Angles)];        
alpha=pi/4;
beta=0;
setting=[1,2,4,3];
SimulateScenario(ThetaT,PhiT,NPulses,d_over_lambda,NoisePower,AntennaCoordinates,setting,alpha,beta)


Angles= [ 0 pi/2 pi -pi/2];
AntennaCoordinates=[zeros(1,length(Angles));cos(Angles);sin(Angles)];        
alpha=pi/4;
beta=-pi/4;
setting=[1,2,4,3];
SimulateScenario(ThetaT,PhiT,NPulses,d_over_lambda,NoisePower,AntennaCoordinates,setting,alpha,beta)


ThetaT = [ 45 ; -45]  *pi/180;
PhiT   = [ 30 ;  30]  *pi/180;

Angles= [ pi/4 3*pi/4 -3*pi/4 -pi/4];
AntennaCoordinates=[zeros(1,length(Angles));cos(Angles);sin(Angles)];        
alpha=pi/4;
beta=0;
setting=[1,2,4,3];
SimulateScenario(ThetaT,PhiT,NPulses,d_over_lambda,NoisePower,AntennaCoordinates,setting,alpha,beta)


Angles= [ 0 pi/2 pi -pi/2];
AntennaCoordinates=[zeros(1,length(Angles));cos(Angles);sin(Angles)];        
alpha=pi/4;
beta=-pi/4;
setting=[1,2,4,3];
SimulateScenario(ThetaT,PhiT,NPulses,d_over_lambda,NoisePower,AntennaCoordinates,setting,alpha,beta)


ThetaT = [  0 ;   0]  *pi/180;
PhiT   = [ 45 ; -45]  *pi/180;

Angles= [ pi/4 3*pi/4 -3*pi/4 -pi/4];
AntennaCoordinates=[zeros(1,length(Angles));cos(Angles);sin(Angles)];        
alpha=pi/4;
beta=0;
setting=[1,2,4,3];
SimulateScenario(ThetaT,PhiT,NPulses,d_over_lambda,NoisePower,AntennaCoordinates,setting,alpha,beta)


Angles= [ 0 pi/2 pi -pi/2];
AntennaCoordinates=[zeros(1,length(Angles));cos(Angles);sin(Angles)];        
alpha=pi/4;
beta=-pi/4;
setting=[1,2,4,3];
SimulateScenario(ThetaT,PhiT,NPulses,d_over_lambda,NoisePower,AntennaCoordinates,setting,alpha,beta)




Angles= [0 pi/3 2*pi/3 pi -2*pi/3 -pi/3];
AntennaCoordinates=[zeros(1,length(Angles));cos(Angles);sin(Angles)];
alpha=[pi/3 pi/3   pi/3];
beta =[  0  pi/3  -pi/3];

setting= [2,3,6,5;
          3,4,1,6;
          1,2,5,4];
NMCS=1000;
MinDistancePhi=0;
MinDistanceTheta=5;
GardBand=30;
PerformanceComparison(NPulses,NMCS,d_over_lambda,MinDistancePhi,MinDistanceTheta,GardBand,NoisePower,AntennaCoordinates,setting,alpha,beta)



Angles= [0 pi/4 pi/2 3*pi/4 pi -3*pi/4 -pi/2 -pi/4];
AntennaCoordinates=[zeros(1,length(Angles));cos(Angles);sin(Angles)];
alpha=[pi/4 pi/4   3*pi/8  3*pi/8 3*pi/8 3*pi/8];
beta =[0  -pi/4  pi/8   -pi/8   3*pi/8 -3*pi/8];

setting= [2,4,8,6;
          1,3,7,5;
          3,4,8,7;
          2,3,7,6;
          4,5,1,8;
          1,2,6,5];
NMCS=1000;
MinDistancePhi=0;
MinDistanceTheta=5;
GardBand=30;
PerformanceComparison(NPulses,NMCS,d_over_lambda,MinDistancePhi,MinDistanceTheta,GardBand,NoisePower,AntennaCoordinates,setting,alpha,beta)


