function [Estimates,Average,MMSE,SimpleAvg]=ComputeEstimates(Y,d_over_lambda,setting,alpha,beta)
    N=size(setting);
    for i=1:N
        [Sol_U,Sol_V]=Compute_VandU(Y(setting(i,:),:),d_over_lambda,alpha(i),beta(i));

        EThetaT=real(asin(Sol_V));
        EPhiT  =real(asin(Sol_U./cos(EThetaT)));

    %     temp=(abs(real(EThetaT(1,:)))>=pi/2-1e-7) | (abs(real(EPhiT(1,:)))>=pi/2-1e-7);
    %     EThetaT(1,temp)=nan;
    %     EPhiT(1,temp)=nan;
    % 
    %     temp=(abs(real(EThetaT(2,:)))>=pi/2-1e-7) | (abs(real(EPhiT(2,:)))>=pi/2-1e-7);
    %     EThetaT(2,temp)=nan;
    %     EPhiT(2,temp)=nan;

        Estimates(:,1:4,i)=[EThetaT(1,:).' EPhiT(1,:).' EThetaT(2,:).' EPhiT(2,:).'];

        FiniteT1=isfinite(Estimates(:,1,i));
        FiniteT2=isfinite(Estimates(:,3,i));
        Average(i,1:2)=mean(Estimates(FiniteT1,[1,2],i));
        Average(i,3:4)=mean(Estimates(FiniteT2,[3,4],i));

        MMSE(i,[1,2])=mean((ones(length(Estimates(FiniteT1,1,i)),1)*Average(i,[1,2])-Estimates(FiniteT1,[1,2],i)).^2);
        MMSE(i,[3,4])=mean((ones(length(Estimates(FiniteT2,3,i)),1)*Average(i,[3,4])-Estimates(FiniteT2,[3,4],i)).^2);
    end
    All_Estimates=reshape(permute(Estimates,[1 3 2]),[],4);
    FiniteT1=isfinite(All_Estimates(:,2));
    FiniteT2=isfinite(All_Estimates(:,4));
    SimpleAvg(1:2)=mean(All_Estimates(FiniteT1,[1,2]));
    SimpleAvg(3:4)=mean(All_Estimates(FiniteT2,[3,4]));