function PerformanceComparison(NPulses,NMCS,d_over_lambda,MinDistancePhi,MinDistanceTheta,GardBand,NoisePower,AntennaCoordinates,setting,alpha,beta)
Area=90-GardBand;
DistanceRange=sqrt(2)*(6:2:180-2*GardBand-2);
[~,NumAntenna]=size(AntennaCoordinates);
[NumSubantenna,~]=size(setting);

Average_ANT=nan(NumSubantenna,4,length(DistanceRange),NMCS);
SimpleAvg=nan(length(DistanceRange),NMCS,4);

for i=1:length(DistanceRange)
    Distance=DistanceRange(i);
    
    parfor r=1:NMCS
        ThetaT=nan(2,1);
        PhiT  =nan(2,1);
        ThetaT(1)=-Area+MinDistanceTheta+ (2*Area-MinDistanceTheta)*rand;
        PhiT  (1)= 2*Area*rand-Area;
        while (sqrt((ThetaT(1)+Area)^2+(PhiT(1)+Area)^2)<Distance) && (sqrt((ThetaT(1)+Area)^2+(PhiT(1)-Area)^2)<Distance)
            ThetaT(1)=-Area+MinDistanceTheta+ (2*Area-MinDistanceTheta)*rand;
            PhiT  (1)=2*Area*rand-Area;
        end
        
        ang=sign(rand-0.5)*(asin(MinDistancePhi/Distance) + (acos(MinDistanceTheta/Distance)-asin(MinDistancePhi/Distance))*rand);
        ThetaT(2)=ThetaT(1)-Distance*cos(ang);
        PhiT  (2)=PhiT  (1)+sign(rand-0.5)*Distance*sin(ang);
        
        while (ThetaT(2)>Area)||(ThetaT(2)<-Area)||(PhiT(2)>Area)||(PhiT(2)<-Area)
            ang=sign(rand-0.5)*(asin(MinDistancePhi/Distance) + (acos(MinDistanceTheta/Distance)-asin(MinDistancePhi/Distance))*rand);
            ThetaT(2)=ThetaT(1)-Distance*cos(ang);
            PhiT  (2)=PhiT  (1)+sign(rand-0.5)*Distance*sin(ang);
        end

        ThetaT=ThetaT*pi/180;
        PhiT=PhiT*pi/180;

        All_ThetaT(i,r,:)=ThetaT;
        All_PhiT(i,r,:)=PhiT;
        
        Ex=[cos(ThetaT).*cos(PhiT) cos(ThetaT).*sin(PhiT) sin(ThetaT)]';
        Beta=exp(1j*randn(2,NPulses));
        
        Y=exp(1j*2*pi*d_over_lambda*AntennaCoordinates'*Ex)*Beta;
        
        
        Y=Y+sqrt(NoisePower/2)*(randn(NumAntenna,NPulses)+1j*randn(NumAntenna,NPulses));
        
        [Estimates,Average_ANT(:,:,i,r),MMSE,SimpleAvg(i,r,:)]=ComputeEstimates(Y,d_over_lambda,setting,alpha,beta);

        [~, SmartAvg(i,r,:)]= SmartAverage(Estimates,MMSE,NumSubantenna);
        
    end
end

for i=1:length(beta)
    MMSE_ANT(:,:,i)=ComputeMMSE(permute(squeeze(Average_ANT(i,:,:,:)),[2 3 1]),All_ThetaT,All_PhiT);
end
MMSE_SimpleAvg=ComputeMMSE(SimpleAvg,All_ThetaT,All_PhiT);
MMSE_SmartAvg=ComputeMMSE(SmartAvg,All_ThetaT,All_PhiT);

figure ;clf; hold on; grid
plot(DistanceRange,sqrt(MMSE_ANT(:,1,1)),'b-.')
plot(DistanceRange,sqrt(MMSE_SimpleAvg(:,1)),'g--')
plot(DistanceRange,sqrt(MMSE_SmartAvg(:,1)),'k-')
for i=2:length(beta)
    plot(DistanceRange,sqrt(MMSE_ANT(:,1,i)),'b-.')
end

legend('Separate Sets','Simple Avg.','Smart Avg.','Location','best')
xlabel('Distance in degrees')
ylabel('Elevation (\theta) MSE in degrees')

set(gcf,'Color','w')
title('MSE Theta')


figure;clf; hold all; grid
plot(DistanceRange,sqrt(MMSE_ANT(:,2,1)),'b-.')
plot(DistanceRange,sqrt(MMSE_SimpleAvg(:,2)),'g--')
plot(DistanceRange,sqrt(MMSE_SmartAvg(:,2)),'k-')
for i=2:length(beta)
    plot(DistanceRange,sqrt(MMSE_ANT(:,2,i)),'b-.')
end

legend('Separate Sets','Simple Avg.','Smart Avg.','Location','best')
xlabel('Distance in degrees')
ylabel('Azimuth (\phi) MSE in degrees')

set(gcf,'Color','w')
title('MSE Phi')


% for i=1:5:length(DistanceRange)
%     figure;clf; axis([-90 90 -90 90]); set(gca,'XTick',-90:10:90,'YTick',-90:10:90,'XMinorTick','on'); grid  
%     hold on; scatter(All_ThetaT(i,:,1)*180/pi,All_PhiT(i,:,1)*180/pi,50,'r')
%     hold on; scatter(All_ThetaT(i,:,2)*180/pi,All_PhiT(i,:,2)*180/pi,50,'b')
% end


